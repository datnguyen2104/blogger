class MicropostsController < ApplicationController
  def index
    @posts = Micropost.paginate(page: params[:page], per_page: 10)
  end

  def show
    @post = Micropost.find_by id: params[:id]
    @comment = Comment.new
  end

  def new
    @post = Micropost.new
  end

  def edit
    @post = Micropost.find(params[:id])
  end

  def update
    @post = Micropost.find(params[:id])
    if @post.update_attributes(micropost_params)
      flash[:success] = 'Edit post successfully'
      redirect_to root_path
    else
      flash[:warning] = 'Errors'
      render :edit
    end
  end

  def create
    @post = current_user.microposts.build(micropost_params)

    if @post.save
      flash[:success] = "Micropost created!"
      redirect_to root_path
    else
      flash.now[:warning] = 'Input again'
      render :new
    end
  end

  def destroy
    @post = current_user.microposts.find_by(id: params[:id])
    if @post.destroy
      flash[:success] = "Micropost deleted"
    else
      flash[:alert] = 'Errors'
    end
    redirect_to root_path
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content)
  end
end
