class StaticPagesController < ApplicationController
  def home
  end

  def new
    if session[:user_id]
      flash[:success] = 'You have already sign in'
      redirect_to root_path
    else
      @user = User.new
    end
  end

  def create
    @user = User.find_by email: params[:user][:email]
      if @user&.authenticate(params[:user][:password])
        if @user.activated?
          log_in(@user)
          params[:user][:remember_me] == '1' ? remember(@user) : forget(@user)
          flash[:success] = 'You sign in successfully'
          redirect_to root_path
        else
          flash.now[:warning] = 'Your account is not activated. Plase active it first and sign in again'
          render :new
        end
      else
        @user = User.new user_params
        flash.now[:danger] = 'Can not found with this email'
        render :new
      end
  end

  private

  def user_params
    params.require(:user).permit :email, :password
  end
end
