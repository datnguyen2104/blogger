class CommentsController < ApplicationController

  def index
    @comment = Comment.new
  end

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      flash[:success] = 'You are comment successfully'
      redirect_to @comment.micropost
      #redirect_back(fallback_location: root_path)
    else
      @post = @comment.micropost
      flash.now[:warning] = 'Errors'
      render 'microposts/show'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :micropost_id)
  end

end
