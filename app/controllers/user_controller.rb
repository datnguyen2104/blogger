class UserController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new user_params

    if @user.save
      UsersMailer.confirm_email(@user).deliver_now
      flash[:success] = 'You sign up successfully'
      redirect_to root_path
    else
      flash.now[:warning] = 'Input again'
      render :new
    end
  end

  def destroy
    log_out(@user)
    flash[:success] = 'You are log out successfully'
    redirect_to root_path
  end

  def active_account
    @user = User.find_by email: params[:email]

    if @user&.authenticated?(:activation, params[:token]) && @user&.active_account
      flash[:success]  =  'You are activated successfully'
    else
      flash[:warning] = 'Active link is valid'
    end
    redirect_to root_path
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
