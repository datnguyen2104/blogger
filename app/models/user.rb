class User < ApplicationRecord
  has_secure_password

  has_many :microposts, dependent: :destroy
  has_many :comments
  
  before_create :generate_activation_token
  before_save { self.email.downcase! }

  attr_accessor :activation_token
  attr_accessor :remember_token
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :name, presence: true, on: create
  validates :email, uniqueness: true, format: { with: VALID_EMAIL_REGEX, message: 'value is invalid'}

  def authenticated?(attr, token)
    digest = self.send("#{attr}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def active_account
    self.update_attributes activated: true, activated_at: Time.now, activation_digest: nil
  end

  def remember
    self.remember_token  = SecureRandom.urlsafe_base64
    self.update_attribute(:remember_digest, BCrypt::Password.create(self.remember_token))
  end

  def forget
    self.update_attribute(:remember_digest, nil)
  end

  private

  def generate_activation_token
    self.activation_token = SecureRandom.urlsafe_base64
    self.activation_digest = BCrypt::Password.create(self.activation_token)
  end
end
