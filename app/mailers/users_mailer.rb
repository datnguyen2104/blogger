class UsersMailer < ApplicationMailer
  default from: 'thanhdat@gmail.com'

  def confirm_email user
    @user = user
    mail(to: @user.email, subject: 'Confirm email, please !!!')
  end
end
