Rails.application.routes.draw do

  root to:  'static_pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  get 'sign_in', to: 'static_pages#new', as: :sign_in
  post 'sign_in', to: 'static_pages#create'


  get 'sign_up', to: 'user#new', as: :sign_up
  post 'sign_up', to: 'user#create'

  delete 'log_out', to: 'user#destroy', as: :log_out

  get 'active_account/:token' => 'user#active_account', as: :active_account

  resources :microposts
  resources :comments
end
